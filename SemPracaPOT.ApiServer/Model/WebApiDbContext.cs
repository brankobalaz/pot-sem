﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SemPracaPOT.Shared.Models.Dictionary;
using SemPracaPOT.Shared.Models.User;

namespace SemPracaPOT.ApiServer.Model
{
    /// <summary>
    /// Databázový kontext pre prácu s databázou.
    /// </summary>
    // Boli použíté pramene:
    // https://www.c-sharpcorner.com/article/authentication-and-authorization-in-asp-net-core-web-api-with-json-web-tokens/
    // https://stackoverflow.com/questions/49214748/many-to-many-self-referencing-relationship
    public class WebApiDbContext : IdentityDbContext<AppUser>
    {
        public DbSet<Word> Words { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<CrosswordDefinition> CrosswordDefinitions { get; set; }

        /// <summary>
        /// Vráti inštanciu WebApiDbContext
        /// </summary>
        /// <param name="options"></param>
        public WebApiDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Translation>()
                .HasKey(x => new {x.WordId, x.TranslationWordId});

            builder.Entity<Translation>()
                .HasOne(x => x.TranslationWord)
                .WithMany(x => x.TranslationsOf)
                .HasForeignKey(x => x.TranslationWordId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Translation>()
                .HasOne(x => x.Word)
                .WithMany(x => x.Translations)
                .HasForeignKey(x => x.WordId);
        }
    }
}