﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SemPracaPOT.ApiServer.Migrations
{
    public partial class CrosswordDefinitionAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CrosswordDefinition",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Definition = table.Column<string>(type: "TEXT", nullable: true),
                    WordId = table.Column<int>(type: "INTEGER", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "TEXT", nullable: false),
                    AppUserId = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CrosswordDefinition", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CrosswordDefinition_AspNetUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CrosswordDefinition_Words_WordId",
                        column: x => x.WordId,
                        principalTable: "Words",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CrosswordDefinition_AppUserId",
                table: "CrosswordDefinition",
                column: "AppUserId");

            migrationBuilder.CreateIndex(
                name: "IX_CrosswordDefinition_WordId",
                table: "CrosswordDefinition",
                column: "WordId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CrosswordDefinition");
        }
    }
}
