﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SemPracaPOT.ApiServer.Model;
using SemPracaPOT.Shared.Models;
using SemPracaPOT.Shared.Models.User;

namespace SemPracaPOT.ApiServer.Controllers
{
    /// <summary>
    /// Kontrolér na správu používateľeov.
    ///
    /// Používateľ musí byť pre prácu s kontrolérom úspešne autentifikovaný a mať rolu Admin.
    /// </summary>
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly WebApiDbContext _context;
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        /// <summary>
        /// Vráti inštanciu UserController
        /// </summary>
        public UserController(WebApiDbContext context, UserManager<AppUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        /// <summary>
        /// Pridá/Odoberie používateľovi rolu Admin.
        /// </summary>
        /// <param name="setRoleDto">Parametre pre nastavenie role</param>
        //  POST api/UserController/setRole
        [HttpPost]
        [Route("setRole")]
        public async Task<IActionResult> SetRole([FromBody] SetRoleDto setRoleDto)
        {
            var user = await _userManager.FindByNameAsync(setRoleDto.Username);
            if (user == null)
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new ResponseDto() {Status = "Error", Message = "User does not exists!"});

            var roleExists = await _roleManager.RoleExistsAsync(setRoleDto.RoleName);
            if (roleExists)
            {
                if (setRoleDto.HasRole)
                {
                    await _userManager.AddToRoleAsync(user, setRoleDto.RoleName);
                    return Ok(new ResponseDto()
                    {
                        Status = "Success",
                        Message = $"User {setRoleDto.Username} added to role {setRoleDto.RoleName} successfully!"
                    });
                }
                else
                {
                    await _userManager.RemoveFromRoleAsync(user, setRoleDto.RoleName);
                    return Ok(new ResponseDto()
                    {
                        Status = "Success",
                        Message = $"User {setRoleDto.Username} removed from role {setRoleDto.RoleName} successfully!"
                    });
                }
            }

            return StatusCode(StatusCodes.Status500InternalServerError,
                new ResponseDto() {Status = "Error", Message = "Unknown error occurred!"});
        }

        /// <summary>
        /// Vráti zoznam všetkých používateľov v databáze.
        /// </summary>
        // GET api/UserController
        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            var users = _userManager.Users;
            var usersWithRoles = new List<UserRole>();

            foreach (var user in users)
            {
                var roles = (await _userManager.GetRolesAsync(user)).ToList();
                usersWithRoles.Add(new UserRole() {Email = user.Email, Username = user.UserName, Roles = roles});
            }

            return Ok(usersWithRoles);
        }

        /// <summary>
        /// Vráti informácie o používateľovi identifikovanom používateľským menom z parametra username.
        /// </summary>
        /// <param name="username">Používateľské meno používateľa</param>
        // GET UserController/user123
        [HttpGet]
        [Route("{username}")]
        public async Task<IActionResult> GetUser(string username)
        {
            var user = _userManager.Users.FirstOrDefault(x => x.UserName == username);

            if (user != null)
            {
                var roles = (await _userManager.GetRolesAsync(user)).ToList();
                var userWithRole = new UserRole() {Email = user.Email, Username = user.UserName, Roles = roles};
                return Ok(userWithRole);
            }
            else
            {
                return BadRequest(new ResponseDto() {Status = "Error", Message = "User does not exists!"});
            }
        }

        /// <summary>
        /// Odstráni používateľa identifikovaného podľa mena v parametri username.
        /// </summary>
        /// <param name="username">Používateľské meno používateľa</param>
        // DELETE api/UserController/user123
        [HttpDelete]
        [Route("{username}")]
        public async Task<IActionResult> DeleteUser(string username)
        {
            var user = _userManager.Users.FirstOrDefault(x => x.UserName == username);

            if (user == null)
                return BadRequest(new ResponseDto() {Status = "Error", Message = "User does not exists!"});

            // Return deleted object to client
            var roles = (await _userManager.GetRolesAsync(user)).ToList();
            var userWithRole = new UserRole() {Email = user.Email, Username = user.UserName, Roles = roles};

            var usersWords = _context.Words?.Where(x => x.AppUserId == user.Id)
                .Include(x => x.Translations)
                .Include(x => x.TranslationsOf)
                .ToList();

            if (usersWords == null)
                return BadRequest(new ResponseDto() {Status = "Error", Message = "User does not have words!"});

            foreach (var word in usersWords)
            {
                word.Translations.Clear();
                word.TranslationsOf.Clear();
                _context.Words.Remove(word);
            }

            await _context.SaveChangesAsync();
            await _userManager.DeleteAsync(user);

            return Ok(userWithRole);
        }

        /// <summary>
        /// Upraví používateľa identifikovaého podľa používateľského mena v parametri username.
        /// </summary>
        /// <param name="username">Používateľské meno</param>
        /// <param name="newUserInfo">Nové parametre používateľa</param>
        // PUT api/UserController/user123
        [HttpPut]
        [Route("{username}")]
        public async Task<IActionResult> EditUser(string username, [FromBody] EditUserDto newUserInfo)
        {
            var user = _userManager.Users.FirstOrDefault(x => x.UserName == username);

            if (user != null)
            {
                if (newUserInfo.Email != null && newUserInfo.Email != user.Email)
                    await _userManager.SetEmailAsync(user, newUserInfo.Email);

                if (newUserInfo.Username != null && newUserInfo.Username != user.UserName)
                    await _userManager.SetUserNameAsync(user, newUserInfo.Username);

                if (newUserInfo.Password != null && !await _userManager.CheckPasswordAsync(user, newUserInfo.Password))
                {
                    await _userManager.RemovePasswordAsync(user);
                    await _userManager.AddPasswordAsync(user, newUserInfo.Password);
                }

                // Return updated object to client
                var roles = (await _userManager.GetRolesAsync(user)).ToList();
                var userWithRole = new UserRole() {Email = user.Email, Username = user.UserName, Roles = roles};

                return Ok(userWithRole);
            }
            else
            {
                return BadRequest(new ResponseDto() {Status = "Error", Message = "User does not exists!"});
            }
        }
    }
}