﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SemPracaPOT.ApiServer.Model;
using SemPracaPOT.Shared.Models;
using SemPracaPOT.Shared.Models.Dictionary;
using SemPracaPOT.Shared.Models.User;

namespace SemPracaPOT.ApiServer.Controllers
{
    /// <summary>
    /// Kontrolér na prácu s jazykmi.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class LanguagesController : ControllerBase
    {
        private readonly WebApiDbContext _dbContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<AppUser> _userManager;

        /// <summary>
        /// Vráti inštanciu LanguagesController
        /// </summary>
        public LanguagesController(WebApiDbContext dbContext, IHttpContextAccessor httpContextAccessor,
            UserManager<AppUser> userManager)
        {
            _userManager = userManager;
            _dbContext = dbContext;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Vráti zoznam jazykov v databáze.
        /// </summary>
        // GET: api/Languages
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Language>>> GetLanguages()
        {
            var languages = await _dbContext.Languages.Include(x => x.AppUser).ToListAsync();
            languages.ForEach(x =>
            {
                var userName = x.AppUser?.UserName;
                x.AppUser = null;
                x.AppUser = new AppUser() {UserName = userName};
            });

            return Ok(languages);
        }

        /// <summary>
        /// Vytvorí nový jazyk.
        ///
        /// Používateľ musí byť úspešne autentifikovaný a mať rolu Admin.
        /// </summary>
        /// <param name="language">Názov nového jazyka</param>
        // POST: api/Languages
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<Language>> PostLanguage([FromBody] Language language)
        {
            var user = _userManager.Users.FirstOrDefault(x =>
                x.UserName == _httpContextAccessor.HttpContext.User.Identity.Name);

            if (user == null)
                return BadRequest(new ResponseDto() {Status = "Error", Message = "User not found!"});

            language.CreatedAt = DateTime.Now;
            language.AppUserId = user.Id;

            var newLanguage = new Language()
            {
                Name = language.Name,
                AppUser = user,
                CreatedAt = language.CreatedAt
            };

            _dbContext.Languages.Add(newLanguage);
            await _dbContext.SaveChangesAsync();

            language.Id = newLanguage.Id;

            return Ok(language);
        }

        /// <summary>
        /// Odstráni jazyk s daným Id z databázy.
        ///
        /// Používateľ musí byť úspešne autentifikovaný a mať rolu Admin.
        /// </summary>
        /// <param name="id">Id jazyka</param>
        // DELETE: api/Languages/5
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLanguage(int id)
        {
            var language = await _dbContext.Languages.FindAsync(id);
            if (language == null)
            {
                return NotFound();
            }

            _dbContext.Languages.Remove(language);
            await _dbContext.SaveChangesAsync();

            return NoContent();
        }
    }
}