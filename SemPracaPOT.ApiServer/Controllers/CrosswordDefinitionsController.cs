﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SemPracaPOT.ApiServer.Model;
using SemPracaPOT.Shared.Models;
using SemPracaPOT.Shared.Models.Dictionary;
using SemPracaPOT.Shared.Models.User;

namespace SemPracaPOT.ApiServer.Controllers
{
    /// <summary>
    /// Kontrolér pre prácu s krížovkárskymi definíciami slov v databáze.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CrosswordDefinitionsController : ControllerBase
    {
        private readonly WebApiDbContext _context;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly UserManager<AppUser> _userManager;

        /// <summary>
        /// Vráti inštanciu CrosswordDefinitionsController
        /// </summary>
        public CrosswordDefinitionsController(WebApiDbContext context, IHttpContextAccessor contextAccessor,
            UserManager<AppUser> userManager)
        {
            _context = context;
            _contextAccessor = contextAccessor;
            _userManager = userManager;
        }

        /// <summary>
        /// Vracia krížovkársku definíciu s daným ID.
        /// </summary>
        /// <param name="id">Id definície</param>
        // GET: api/CrosswordDefinitions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CrosswordDefinition>> GetCrosswordDefinition(int id)
        {
            var crosswordDefinition = await _context.CrosswordDefinitions.FindAsync(id);

            if (crosswordDefinition == null)
                return NotFound();

            return crosswordDefinition;
        }

        /// <summary>
        /// Vracia všetky krížovkárske definície pre slovo s daným ID.
        ///
        /// Ak slovo s danám Id neexistuje, vráti BadRequest
        /// </summary>
        /// <param name="wordId">Id slova</param>
        // GET: api/CrosswordDefinitions/forWord/5
        [HttpGet("forWord/{wordId}")]
        public async Task<ActionResult<List<CrosswordDefinition>>> GetCrosswordDefinitionsForWord(int wordId)
        {
            var word = await _context.Words
                .Include(x => x.CrosswordDefinitions)
                .ThenInclude(d => d.AppUser)
                .FirstOrDefaultAsync(x => x.Id == wordId);

            if (word is not {CrosswordDefinitions: { }})
                return BadRequest();

            var definitions = word.CrosswordDefinitions.ToList();

            definitions.ForEach(x => x.Word = null);
            definitions.ForEach(x =>
            {
                var userName = x.AppUser?.UserName;
                x.AppUser = null;
                x.AppUser = new AppUser() {UserName = userName};
            });

            return definitions;
        }

        /// <summary>
        /// Vráti všetky slová s definíciami, ktorých definície obsahujú string def.
        /// </summary>
        /// <param name="def">Hľadaný string</param>
        /// <returns></returns>
        // GET: api/CrosswordDefinitions/forDefinition/?def=definition
        [HttpGet("forDefinition")]
        public async Task<ActionResult<List<Word>>> GetWordsForDefinition([FromQuery] string def)
        {
            var words = await _context.Words
                .Include(x => x.CrosswordDefinitions)
                .ThenInclude(d => d.AppUser)
                .Include(x => x.Language)
                .Where(x => x.CrosswordDefinitions.Any(d => d.Definition.Contains(def)))
                .ToListAsync();

            foreach (var word in words)
            {
                if (word.CrosswordDefinitions == null)
                    continue;

                word.CrosswordDefinitions = word.CrosswordDefinitions.Where(d => d.Definition.Contains(def)).ToList();

                foreach (var definition in word.CrosswordDefinitions)
                {
                    var userName = definition.AppUser?.UserName;
                    definition.AppUser = null;
                    definition.AppUser = new AppUser() {UserName = userName};
                    definition.Word = null;
                }

                word.Language.AppUser = null;
                word.AppUser = null;
            }

            return words;
        }

        /// <summary>
        /// Aktualizuje danú krížovkársku definíciu.
        ///
        /// Pokiaľ definícia s daným Id neexistuje, vráti BadRequest.
        /// </summary>
        /// <param name="id">Id definície</param>
        /// <param name="crosswordDefinition">Nová definícia</param>
        // PUT: api/CrosswordDefinitions/5
        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrosswordDefinition(int id, CrosswordDefinition crosswordDefinition)
        {
            if (id != crosswordDefinition.Id)
            {
                return BadRequest();
            }

            var userName = _contextAccessor.HttpContext?.User.Identity?.Name;
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == userName);
            if (crosswordDefinition is not {AppUserId: { }} || user == null) return BadRequest();

            if (!(await _userManager.IsInRoleAsync(user, "Admin") || crosswordDefinition.AppUserId.Equals(user.Id)))
                return BadRequest(new ResponseDto() {Status = "Error", Message = "Unauthorized!"});

            crosswordDefinition.AppUser = user;
            _context.Entry(crosswordDefinition).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CrosswordDefinitionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Vytvorí novú definíciu.
        ///
        /// Používateľ musí byť úspešne autentifikovaný.
        /// </summary>
        /// <param name="crosswordDefinition">Nová definícia</param>
        // POST: api/CrosswordDefinitions
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<CrosswordDefinition>> PostCrosswordDefinition(
            CrosswordDefinition crosswordDefinition)
        {
            crosswordDefinition.CreatedAt = DateTime.Now;

            var userName = _contextAccessor.HttpContext?.User.Identity?.Name;
            crosswordDefinition.AppUser = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == userName);
            if (crosswordDefinition.AppUser == null) return BadRequest();

            _context.CrosswordDefinitions.Add(crosswordDefinition);
            await _context.SaveChangesAsync();

            crosswordDefinition.AppUser = null;

            return CreatedAtAction("GetCrosswordDefinition", new {id = crosswordDefinition.Id}, crosswordDefinition);
        }

        /// <summary>
        /// Odstráni definíciu s daným Id z databázy.
        ///
        /// Používateľ musí byť úspešne autentifikovaný.
        /// </summary>
        /// <param name="id">Id definície</param>
        // DELETE: api/CrosswordDefinitions/5
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCrosswordDefinition(int id)
        {
            var crosswordDefinition = await _context.CrosswordDefinitions.FindAsync(id);
            if (crosswordDefinition == null)
            {
                return NotFound();
            }

            var userName = _contextAccessor.HttpContext?.User.Identity?.Name;
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == userName);
            if (crosswordDefinition is not {AppUserId: { }} || user == null) return BadRequest();

            if (!(await _userManager.IsInRoleAsync(user, "Admin") || crosswordDefinition.AppUserId.Equals(user.Id)))
                return BadRequest(new ResponseDto() {Status = "Error", Message = "Unauthorized!"});

            _context.CrosswordDefinitions.Remove(crosswordDefinition);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CrosswordDefinitionExists(int id)
        {
            return _context.CrosswordDefinitions.Any(e => e.Id == id);
        }
    }
}