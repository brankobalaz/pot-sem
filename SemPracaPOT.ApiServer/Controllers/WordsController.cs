﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SemPracaPOT.ApiServer.Model;
using SemPracaPOT.Shared.Models;
using SemPracaPOT.Shared.Models.Dictionary;
using SemPracaPOT.Shared.Models.User;

namespace SemPracaPOT.ApiServer.Controllers
{
    /// <summary>
    /// Kontrolér pre prácu so slovami.
    ///
    /// Používateľ musí byť úspešne autentifikovaný.
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class WordsController : ControllerBase
    {
        private readonly WebApiDbContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<AppUser> _userManager;

        /// <summary>
        /// Veáti inštanciu WordsController.
        /// </summary>
        public WordsController(WebApiDbContext context, IHttpContextAccessor httpContextAccessor,
            UserManager<AppUser> userManager)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
        }

        /// <summary>
        /// Vráti všetky slová z databázy.
        ///
        /// Vrátane referenčných polí jazyka a používateľa.
        /// </summary>
        // GET: api/Words
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Word>>> GetWords()
        {
            var words = await _context.Words
                .Include(x => x.Language)
                .Include(x => x.AppUser)
                .ToListAsync();
            words.ForEach(x =>
            {
                var userName = x.AppUser.UserName;
                x.AppUser = new AppUser() {UserName = userName};
                x.Language.AppUser = null;
            });

            return words;
        }

        /// <summary>
        /// Vráti slová, kroré vytvoril používateľ, ktorý autentifikoval požiadavku.
        /// </summary>
        // GET: api/Words/myWords
        [HttpGet("myWords")]
        public async Task<ActionResult<IEnumerable<Word>>> GetWordsOfUser()
        {
            var user = _userManager.Users.FirstOrDefault(x =>
                x.UserName == _httpContextAccessor.HttpContext.User.Identity.Name);

            return await _context.Words
                .Where(x => x.AppUser.UserName.Equals(user.UserName))
                .Include(x => x.Language)
                .Include(x => x.AppUser)
                .ToListAsync();
        }

        /// <summary>
        /// Vráti záznam slova identifikovaného pomocou Id.
        /// </summary>
        /// <param name="id">Id slova</param>
        // GET: api/Words/5
        [HttpGet("{id}")]
        public ActionResult<Word> GetWord(int id)
        {
            var word = _context.Words.Where(x => x.Id == id)
                .Include(x => x.AppUser)
                .Include(x => x.Language.AppUser)
                .FirstOrDefault();

            if (word == null)
            {
                return NotFound();
            }

            var wordDto = word;

            return wordDto;
        }

        /// <summary>
        /// Upraví záznam slova podľa parametra newWord.
        /// </summary>
        /// <param name="id">Id slova</param>
        /// <param name="newWord">Nové parametre slova</param>
        // PUT: api/Words/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWord(int id, [FromBody] Word newWord)
        {
            if (id != newWord.Id)
                return BadRequest();

            var word = await _context.Words.FindAsync(id);
            var language = await _context.Languages.FindAsync(newWord.LanguageId);
            var user = _userManager.Users.FirstOrDefault(x =>
                x.UserName == _httpContextAccessor.HttpContext.User.Identity.Name);

            if (word == null || language == null)
                return BadRequest(new ResponseDto() {Status = "Error", Message = "User or word not found!"});

            if (!(await _userManager.IsInRoleAsync(user, "Admin") || word.AppUserId.Equals(user?.Id)))
                return BadRequest(new ResponseDto() {Status = "Error", Message = "Unauthorized!"});

            word.Content = newWord.Content;
            word.Description = newWord.Description;
            word.Language = language;

            _context.Entry(word).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WordExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Vytvorí nové slovo podľa parametra newWord.
        /// </summary>
        /// <param name="newWordDto">Parametre nového slova</param>
        // POST: api/Words
        [HttpPost]
        public async Task<ActionResult<Word>> PostWord(Word newWordDto)
        {
            var user = _userManager.Users.FirstOrDefault(x =>
                x.UserName == _httpContextAccessor.HttpContext.User.Identity.Name);

            if (user == null)
                return BadRequest(new ResponseDto() {Status = "Error", Message = "User not found!"});

            var language = _context.Languages.Where(l => l.Id == newWordDto.LanguageId).Include(x => x.AppUser)
                .FirstOrDefault();

            if (language == null)
                return BadRequest(new ResponseDto() {Status = "Error", Message = "Language not found!"});

            newWordDto.CreatedAt = DateTime.Now;
            newWordDto.AppUserId = user.Id;
            newWordDto.Language = language;

            var newWord = new Word
            {
                Content = newWordDto.Content,
                Description = newWordDto.Description,
                Language = language,
                AppUser = user,
                CreatedAt = newWordDto.CreatedAt
            };

            _context.Words.Add(newWord);
            await _context.SaveChangesAsync();

            return Ok(newWordDto);
        }

        /// <summary>
        /// Odstráni z databázy slovo identifikované pomocou Id.
        /// </summary>
        /// <param name="id">Id slova</param>
        // DELETE: api/Words/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWord(int id)
        {
            var user = _userManager.Users.FirstOrDefault(x =>
                x.UserName == _httpContextAccessor.HttpContext.User.Identity.Name);

            var word = await _context.Words
                .Where(x => x.Id == id)
                .Include(x => x.Translations)
                .Include(x => x.TranslationsOf)
                .FirstOrDefaultAsync();

            if (user == null || word == null)
                return BadRequest(new ResponseDto() {Status = "Error", Message = "User or word not found!"});

            if (!(await _userManager.IsInRoleAsync(user, "Admin") || word.AppUserId.Equals(user.Id)))
                return BadRequest(new ResponseDto() {Status = "Error", Message = "Not authorized!"});

            word.Translations.Clear();
            word.TranslationsOf.Clear();

            _context.Words.Remove(word);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool WordExists(int id)
        {
            return _context.Words.Any(e => e.Id == id);
        }
    }
}