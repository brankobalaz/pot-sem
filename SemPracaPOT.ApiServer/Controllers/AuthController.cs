﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SemPracaPOT.Shared.Models;
using SemPracaPOT.Shared.Models.User;

namespace SemPracaPOT.ApiServer.Controllers
{
    /// <summary>
    /// Autentifikačný kontrolér.
    ///
    /// Poskytuje nástroje na vytvorenie používateľa, overenie identity a poskytnutie autentifikačného tokenu klientovi.
    /// Kontrolér obsahovo prebratý z: https://www.c-sharpcorner.com/article/authentication-and-authorization-in-asp-net-core-web-api-with-json-web-tokens/
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Vytvorí inštanciu AuthController.
        /// </summary>
        public AuthController(UserManager<AppUser> userManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _configuration = configuration;
        }

        /// <summary>
        /// Autentifiku používateľa a v prípade úspechu vráti JWT token.
        ///
        /// V prípade neúspešnej autentifikácie vráti Unauthorized.
        /// </summary>
        /// <param name="loginDto">Identifikácia používateľa</param>
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginDto loginDto)
        {
            var user = await _userManager.FindByNameAsync(loginDto.Username);
            if (user != null && await _userManager.CheckPasswordAsync(user, loginDto.Password))
            {
                var userRoles = await _userManager.GetRolesAsync(user);

                var authClaims = new List<Claim>
                {
                    new(ClaimTypes.Name, user.UserName),
                    new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }

                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["Jwt:Issuer"],
                    audience: _configuration["Jwt:Audience"],
                    expires: DateTime.Now.AddDays(7),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

                var response = new LoginResponseDto()
                {
                    Token = new JwtSecurityTokenHandler().WriteToken(token),
                    Expiration = token.ValidTo,
                    Username = user.UserName,
                    Roles = userRoles,
                    Id = user.Id
                };

                return Ok(response);
            }

            return Unauthorized();
        }

        /// <summary>
        /// Vytvorí nového používateľa.
        ///
        /// Pokiaľ existuje používateľ s rovnakým menom, nový používateľ sa nevytvorí a metóda vráti status code 500.
        /// </summary>
        /// <param name="registerDto">Identifikácia vytváraného používateľa</param>
        /// <returns></returns>
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterDto registerDto)
        {
            var userExists = await _userManager.FindByNameAsync(registerDto.Username);
            if (userExists != null)
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new ResponseDto() {Status = "Error", Message = "User already exists!"});

            var user = new AppUser()
            {
                Email = registerDto.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = registerDto.Username
            };
            var result = await _userManager.CreateAsync(user, registerDto.Password);
            if (!result.Succeeded)
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new ResponseDto()
                        {Status = "Error", Message = "User creation failed! Please check user details and try again."});

            return Ok(new ResponseDto() {Status = "Success", Message = "User created successfully!"});
        }
    }
}