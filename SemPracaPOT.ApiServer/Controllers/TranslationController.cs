﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SemPracaPOT.ApiServer.Model;
using SemPracaPOT.Shared.Models;
using SemPracaPOT.Shared.Models.Dictionary;
using SemPracaPOT.Shared.Models.User;

namespace SemPracaPOT.ApiServer.Controllers
{
    /// <summary>
    /// Kontrolér na prácu s prekladmi.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TranslationController : ControllerBase
    {
        private readonly WebApiDbContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<AppUser> _userManager;
        private const int NotDefined = 0;

        /// <summary>
        /// Vráti inštanciu TranslationController
        /// </summary>
        public TranslationController(WebApiDbContext context, IHttpContextAccessor httpContextAccessor,
            UserManager<AppUser> userManager)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
        }

        /// <summary>
        /// Vyhľadá zadané slovo v databáze slov daného jazyka a vráti jeho preklady do zvoleného jazyka.
        /// </summary>
        /// <param name="wordString">Hľadané slovo</param>
        /// <param name="languageFrom">Jazyk hľadaného slova</param>
        /// <param name="languageTo">Jazyk prekladov</param>
        // GET: api/Translation/?wordString=wordToTranslate&languageFrom=2&languageTo=5
        [HttpGet]
        public async Task<ActionResult<TranslationDataDto>> GetTranslations([FromQuery] string wordString,
            [FromQuery] int languageFrom, [FromQuery] int languageTo)
        {
            var word = await _context.Words.Where(x => x.Content.Equals(wordString) && x.LanguageId == languageFrom)
                .Include(x => x.AppUser)
                .Include(x => x.Language.AppUser)
                .Include(x => x.Translations)
                .ThenInclude(t => t.TranslationWord.Language)
                .Include(x => x.Translations)
                .ThenInclude(t => t.AppUser)
                .Include(x => x.TranslationsOf)
                .ThenInclude(to => to.Word.Language)
                .Include(x => x.TranslationsOf)
                .ThenInclude(to => to.AppUser)
                .FirstOrDefaultAsync();

            if (word == null)
            {
                return NotFound();
            }

            return FindTranslations(word, languageTo);
        }

        /// <summary>
        /// Vráti všetky preklady daného slova.
        /// </summary>
        /// <param name="id">Id slova</param>
        // GET: api/Translation/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TranslationDataDto>> GetAllTranslations(int id)
        {
            var word = await _context.Words.Where(x => x.Id == id)
                .Include(x => x.AppUser)
                .Include(x => x.Language.AppUser)
                .Include(x => x.Translations)
                .ThenInclude(t => t.TranslationWord.Language)
                .Include(x => x.Translations)
                .ThenInclude(t => t.AppUser)
                .Include(x => x.TranslationsOf)
                .ThenInclude(to => to.Word.Language)
                .Include(x => x.TranslationsOf)
                .ThenInclude(to => to.AppUser)
                .FirstOrDefaultAsync();


            if (word == null)
            {
                return NotFound();
            }

            return FindTranslations(word, NotDefined);
        }

        /// <summary>
        /// Vráti slová na ktoré ešte neexistuje preklad zo slova zadaného pomocou parametra Id.
        /// </summary>
        /// <param name="id">Id slova</param>
        // GET: api/Translation/possibleTranslations/5
        [HttpGet("possibleTranslations/{id}")]
        public ActionResult<List<Word>> GetPossibleTranslations(int id)
        {
            var words = _context.Words
                .Where(x => x.Translations.All(t => t.TranslationWordId != id) &&
                            x.TranslationsOf.All(t => t.WordId != id) &&
                            x.Id != id)
                .Include(x => x.Language)
                .ToList();

            return Ok(words);
        }

        /// <summary>
        /// Nájde všetky preklady slova zadaného parametrom word do jazyka zadaného parametrom languageTo.
        /// </summary>
        /// <param name="word">Slovo</param>
        /// <param name="languageTo">Jazyk prekladov</param>
        private static ActionResult<TranslationDataDto> FindTranslations(Word word, int languageTo)
        {
            var translationDataDto = new TranslationDataDto()
            {
                WordId = word.Id,
                Word = word,
                LanguageIdTo = languageTo
            };

            var translations = new List<Translation>();

            foreach (var translation in word.Translations)
            {
                if (translation.TranslationWord.LanguageId == languageTo || languageTo == 0)
                {
                    translations.Add(translation);
                }
            }

            foreach (var translation in word.TranslationsOf)
            {
                if (translation.Word.LanguageId == languageTo || languageTo == 0)
                {
                    // Get right order
                    (translation.TranslationWord, translation.Word) = (translation.Word, translation.TranslationWord);
                    (translation.TranslationWordId, translation.WordId) =
                        (translation.WordId, translation.TranslationWordId);

                    translations.Add(translation);
                }
            }

            translations.ForEach(ClearTranslationForSend);
            translationDataDto.Translations = translations;

            return translationDataDto;
        }

        /// <summary>
        /// Odstráni z prekladu cyklické vzťahy a tým pripravý preklad na odoslanie klientovi.
        ///
        /// Neupravuje záznam v databáze.
        /// </summary>
        /// <param name="translation">Preklad</param>
        private static void ClearTranslationForSend(Translation translation)
        {
            translation.Word.Translations = null;
            translation.Word.TranslationsOf = null;

            translation.TranslationWord.Translations = null;
            translation.TranslationWord.TranslationsOf = null;

            translation.AppUser = new AppUser() {UserName = translation.AppUser.UserName};
            translation.Word.AppUser = new AppUser() {UserName = translation.AppUser.UserName};
            translation.TranslationWord.AppUser = new AppUser() {UserName = translation.AppUser.UserName};
        }

        /// <summary>
        /// Vytvorí nový preklad.
        ///
        /// Používateľ musí byť úspešne autentifikovaný.
        /// </summary>
        /// <param name="translationDataDto">Dáta požadovaného prekladu</param>
        // POST: api/Translation
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<TranslationDataDto>> PostWord([FromBody] TranslationDataDto translationDataDto)
        {
            var word = _context.Words
                .Where(x => x.Id == translationDataDto.WordId)
                .Include(x => x.Translations)
                .Include(x => x.TranslationsOf)
                .FirstOrDefault();

            var user = _userManager.Users.FirstOrDefault(x =>
                x.UserName == _httpContextAccessor.HttpContext.User.Identity.Name);

            if (word == null || user == null || translationDataDto.Translations == null)
                return BadRequest(new ResponseDto()
                    {Status = "Error", Message = "User or word or translations not found!"});

            translationDataDto.Word = word;

            foreach (var translation in translationDataDto.Translations)
            {
                var translationWord = await _context.Words.FindAsync(translation.TranslationWordId);

                if (translationWord == null)
                    continue;

                // Translation already exixts
                if (word.Translations.FirstOrDefault(x => x.TranslationWordId == translation.TranslationWordId) !=
                    null || word.TranslationsOf.FirstOrDefault(x => x.WordId == translation.TranslationWordId) != null)
                    continue;

                translation.TranslationWord = translationWord;
                translation.CreatedAt = DateTime.Now;

                word.Translations.Add(new Translation()
                {
                    AppUser = user,
                    CreatedAt = translation.CreatedAt,
                    Word = word,
                    TranslationWord = translationWord
                });
            }

            _context.Entry(word).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WordExists(word.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return translationDataDto;
        }

        /// <summary>
        /// Odstráni preklad zadaný pomocou parametrov.
        ///
        /// Používateľ musí byť úspešne autentifikovaný.
        /// </summary>
        /// <param name="wordAId">Id slova A prekladu</param>
        /// <param name="wordBId">Id slova B prekladu</param>
        // DELETE: api/Translation/5?wordBId=6
        [HttpDelete("{wordAId}")]
        public async Task<IActionResult> DeleteTranslation(int wordAId, [FromQuery] int wordBId)
        {
            var wordA = await _context.Words.Where(x => x.Id == wordAId)
                .Include(x => x.Translations)
                .Include(x => x.TranslationsOf)
                .FirstOrDefaultAsync();

            var user = _userManager.Users.FirstOrDefault(x =>
                x.UserName == _httpContextAccessor.HttpContext.User.Identity.Name);

            if (wordA == null)
            {
                return NotFound();
            }

            var translation = wordA.Translations.FirstOrDefault(x => x.TranslationWordId == wordBId) ?? wordA.TranslationsOf.FirstOrDefault(x => x.WordId == wordBId);

            if (translation == null)
                return BadRequest(new ResponseDto() {Status = "Error", Message = "Translation not found!"});

            if (!(await _userManager.IsInRoleAsync(user, "Admin") || translation.AppUserId.Equals(user?.Id)))
                return BadRequest(new ResponseDto() {Status = "Error", Message = "Unauthorized!"});

            if (translation.WordId == wordAId)
                wordA.Translations.Remove(translation);
            else
                wordA.TranslationsOf.Remove(translation);

            _context.Entry(wordA).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WordExists(wordA.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        private bool WordExists(int id)
        {
            return _context.Words.Any(e => e.Id == id);
        }
    }
}