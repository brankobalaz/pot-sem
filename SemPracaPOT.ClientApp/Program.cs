using Blazored.SessionStorage;
using Blazorise;
using Blazorise.Bootstrap;
using Blazorise.Icons.FontAwesome;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using SemPracaPOT.ClientApp;
using SemPracaPOT.ClientApp.Services;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(_ => new HttpClient {BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)});
builder.Services.AddScoped<AuthService>();
builder.Services.AddScoped<UserService>();
builder.Services.AddScoped<WordService>();
builder.Services.AddScoped<TranslationService>();
builder.Services.AddScoped<LanguageService>();
builder.Services.AddScoped<CrosswordDefinitionService>();
builder.Services.AddBlazoredSessionStorage();
builder.Services
    .AddBlazorise(options => { options.ChangeTextOnKeyPress = true; })
    .AddBootstrapProviders()
    .AddFontAwesomeIcons();

await builder.Build().RunAsync();