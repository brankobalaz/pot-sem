﻿using System.Net.Http.Json;
using SemPracaPOT.Shared.Models.Dictionary;

namespace SemPracaPOT.ClientApp.Services
{
    /// <summary>
    /// Služba na správu slov.
    /// </summary>
    public class WordService
    {
        private readonly HttpClient _http;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Vráti inštanciu WordService
        /// </summary>
        public WordService(HttpClient http, IConfiguration configuration)
        {
            _http = http;
            _configuration = configuration;
        }

        /// <summary>
        /// Odošle na server požiadavku o odstránenie slova.
        /// </summary>
        /// <param name="id">Id slova</param>
        public async Task<bool> DeleteWord(int id)
        {
            var response = await _http.DeleteAsync($"{_configuration["ApiServerUrl"]}api/Words/{id}");
            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Vráti zoznam všetkých slov v databáze servera.
        /// </summary>
        public async Task<List<Word>> GetAllWords()
        {
            var response = await _http.GetAsync($"{_configuration["ApiServerUrl"]}api/Words");

            if (response.IsSuccessStatusCode && response is {Content: { }})
            {
                var result = await response.Content.ReadFromJsonAsync<List<Word>>();
                return result ?? new();
            }
            else
            {
                return new();
            }
        }

        /// <summary>
        /// Odošle na server požiadavku o pridanie slova definovaného parameterom newWord do databázy.
        /// </summary>
        /// <param name="newWord">Parametre nového slova</param>
        public async Task<bool> AddWord(Word newWord)
        {
            var response = await _http.PostAsJsonAsync($"{_configuration["ApiServerUrl"]}api/Words", newWord);
            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Získa zo servera záznam slova definovaného pomocou parametra wordId.
        /// </summary>
        /// <param name="wordId">Id slova</param>
        public async Task<Word> GetWord(int wordId)
        {
            var response = await _http.GetAsync($"{_configuration["ApiServerUrl"]}api/Words/{wordId}");

            if (response.IsSuccessStatusCode && response is {Content: { }})
            {
                var result = await response.Content.ReadFromJsonAsync<Word>();
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Odošle na server požiadavku o úpravu slova.
        /// </summary>
        /// <param name="newWord">Parametre upraveného slova</param>
        /// <returns></returns>
        public async Task<bool> EditWord(Word newWord)
        {
            var response =
                await _http.PutAsJsonAsync($"{_configuration["ApiServerUrl"]}api/Words/{newWord.Id}", newWord);
            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Vráti zoznam slov, ktoré vytvoril prihlásený používateľ.
        /// </summary>
        public async Task<List<Word>> GetMyWords()
        {
            var response = await _http.GetAsync($"{_configuration["ApiServerUrl"]}api/Words/myWords");

            if (response.IsSuccessStatusCode && response is {Content: { }})
            {
                var result = await response.Content.ReadFromJsonAsync<List<Word>>();
                return result ?? new();
            }
            else
            {
                return new();
            }
        }
    }
}