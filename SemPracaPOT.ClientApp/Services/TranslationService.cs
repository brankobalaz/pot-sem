﻿using System.Net.Http.Json;
using System.Web;
using SemPracaPOT.Shared.Models.Dictionary;

namespace SemPracaPOT.ClientApp.Services
{
    /// <summary>
    /// Služba pre prácu s prekladmi.
    /// </summary>
    public class TranslationService
    {
        private readonly HttpClient _http;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Vráti inštanciu TranslationService
        /// </summary>
        public TranslationService(HttpClient http, IConfiguration configuration)
        {
            _http = http;
            _configuration = configuration;
        }

        /// <summary>
        /// Vráti zo servera všetky preklady slova zadaného pomocou parametra wordId.
        /// </summary>
        /// <param name="wordId">Id slova</param>
        public async Task<TranslationDataDto> GetAllTanslatinos(int wordId)
        {
            var response = await _http.GetAsync($"{_configuration["ApiServerUrl"]}api/Translation/{wordId}");

            if (response.IsSuccessStatusCode && response is {Content: { }})
            {
                var result = await response.Content.ReadFromJsonAsync<TranslationDataDto>();
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Vráti zo servera všetky preklady daného slova zo zadaného jazyka languageFromId do zadaného jazyka languageToId.
        /// </summary>
        /// <param name="wordFrom">Hľadané slovo</param>
        /// <param name="languageFromId">Id jazka hľadaného slova</param>
        /// <param name="languageToId">Id jazyka prekladov</param>
        // Boli použité poznatky z:
        // https://stackoverflow.com/a/17096289
        public async Task<TranslationDataDto> GetTanslations(string wordFrom, int languageFromId, int languageToId)
        {
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["wordString"] = wordFrom;
            query["languageFrom"] = languageFromId.ToString();
            query["languageTo"] = languageToId.ToString();
            var queryString = query.ToString();

            var response = await _http.GetAsync($"{_configuration["ApiServerUrl"]}api/Translation/?{queryString}");

            if (response.IsSuccessStatusCode && response is {Content: { }})
            {
                var result = await response.Content.ReadFromJsonAsync<TranslationDataDto>();
                return result ?? new() {Translations = new()};
            }
            else
            {
                return new() {Translations = new()};
            }
        }

        /// <summary>
        /// Odošle na server požiadavku na vymazanie prekladu slov identifikovaných pomocou ich Id.
        /// </summary>
        /// <param name="wordAId">Id slova A</param>
        /// <param name="wordBId">Id slova B</param>
        public async Task<bool> DeleteTranslation(int wordAId, int wordBId)
        {
            var response =
                await _http.DeleteAsync(
                    $"{_configuration["ApiServerUrl"]}api/Translation/{wordAId}?wordBId={wordBId}");

            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Odošle na server požiadavku o vytvorenie prekladu slov podľa ich Id.
        /// </summary>
        /// <param name="wordAId">Id slova A</param>
        /// <param name="wordBId">Id slova B</param>
        public async Task<bool> CreateTranslation(int wordAId, int wordBId)
        {
            var translationData = new TranslationDataDto
            {
                WordId = wordAId,
                Translations = new List<Translation>()
                    {new() {TranslationWordId = wordBId}}
            };

            var response =
                await _http.PostAsJsonAsync($"{_configuration["ApiServerUrl"]}api/Translation", translationData);

            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Vráti zo servera zoznam slov, do ktorých ešte neexistuje preklad zo slova zadaného pomocou parametra wordId.
        /// </summary>
        /// <param name="wordId">Id slova</param>
        public async Task<List<Word>> GetPossibleTranslations(int wordId)
        {
            var response =
                await _http.GetAsync($"{_configuration["ApiServerUrl"]}api/Translation/possibleTranslations/{wordId}");

            if (response.IsSuccessStatusCode && response is {Content: { }})
            {
                var result = await response.Content.ReadFromJsonAsync<List<Word>>();
                return result;
            }
            else
            {
                return null;
            }
        }
    }
}