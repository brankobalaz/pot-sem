﻿using System.Net.Http.Json;
using System.Web;
using SemPracaPOT.Shared.Models.Dictionary;

namespace SemPracaPOT.ClientApp.Services
{
    /// <summary>
    /// Služba na správu krížovkárskych definícií.
    /// </summary>
    public class CrosswordDefinitionService
    {
        private readonly IConfiguration _configuration;
        private readonly HttpClient _http;

        /// <summary>
        /// Vráti inštanciu CrosswordDefinitionService
        /// </summary>
        /// <param name="httpClient"></param>
        /// <param name="configuration"></param>
        public CrosswordDefinitionService(HttpClient httpClient, IConfiguration configuration)
        {
            _configuration = configuration;
            _http = httpClient;
        }

        /// <summary>
        /// Vráti zo servera zoznam všetkých krížovkárskych definícií pre slovo zadané pomocou parametra wordId.
        /// </summary>
        /// <param name="wordId">Id slova</param>
        public async Task<List<CrosswordDefinition>> GetWordDefinitions(int wordId)
        {
            var response =
                await _http.GetAsync($"{_configuration["ApiServerUrl"]}api/CrosswordDefinitions/forWord/{wordId}");

            return await response.Content.ReadFromJsonAsync<List<CrosswordDefinition>>();
        }

        /// <summary>
        /// Odošle na server požiadavku o vytvorenie novej definície podľa parametra definition.
        /// </summary>
        /// <param name="definition">Parametre novej definície</param>
        public async Task<bool> AddDefinition(CrosswordDefinition definition)
        {
            var response = await _http.PostAsJsonAsync($"{_configuration["ApiServerUrl"]}api/CrosswordDefinitions",
                definition);
            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Odošle na server požiadavku o úpravu definície podľa parametra definition.
        /// </summary>
        /// <param name="definition">Parametre novej definície</param>
        public async Task<bool> UpdateDefinition(CrosswordDefinition definition)
        {
            var response =
                await _http.PutAsJsonAsync($"{_configuration["ApiServerUrl"]}api/CrosswordDefinitions/{definition.Id}",
                    definition);
            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Odošle na server pžiadavku o úpravu definície podľa parametra definition.
        /// </summary>
        /// <param name="definition">Parametre upravenej definície</param>
        public async Task<bool> DeleteDefinition(CrosswordDefinition definition)
        {
            var response =
                await _http.DeleteAsync($"{_configuration["ApiServerUrl"]}api/CrosswordDefinitions/{definition.Id}");
            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Vráti zo servera zoznam slov, ktoré obsahujú v definícií reťazec zadaný parametrom definition.
        /// </summary>
        /// <param name="definition"></param>
        // Boli použité poznatky z:
        // https://stackoverflow.com/a/17096289
        public async Task<List<Word>> GetWordsForDefinition(string definition)
        {
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["def"] = definition;
            var queryString = query.ToString();

            var response =
                await _http.GetAsync(
                    $"{_configuration["ApiServerUrl"]}api/CrosswordDefinitions/forDefinition/?{queryString}");

            if (response.IsSuccessStatusCode && response is {Content: { }})
            {
                var result = await response.Content.ReadFromJsonAsync<List<Word>>();
                return result ?? new();
            }
            else
            {
                return new();
            }
        }
    }
}