﻿using System.Net.Http.Json;
using SemPracaPOT.Shared.Models.Dictionary;

namespace SemPracaPOT.ClientApp.Services
{
    /// <summary>
    /// Služba na správu jazykov.
    /// </summary>
    public class LanguageService
    {
        private readonly HttpClient _http;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Vráti inštanciu LanguageService
        /// </summary>
        public LanguageService(HttpClient http, IConfiguration configuration)
        {
            _http = http;
            _configuration = configuration;
        }

        /// <summary>
        /// Odošle na server požiadavku o vymazanie jazyka identifikvaného podľa Id.
        /// </summary>
        /// <param name="id">Id jazyka</param>
        public async Task<bool> DeleteLanguage(int id)
        {
            var response = await _http.DeleteAsync($"{_configuration["ApiServerUrl"]}api/Languages/{id}");
            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Vráti zoznam všetkých jazykov z databázy servera.
        /// </summary>
        public async Task<List<Language>> GetAllLanguages()
        {
            var response = await _http.GetAsync($"{_configuration["ApiServerUrl"]}api/Languages");

            if (response.IsSuccessStatusCode && response is {Content: { }})
            {
                var result = await response.Content.ReadFromJsonAsync<List<Language>>();
                return result ?? new();
            }
            else
            {
                return new();
            }
        }

        /// <summary>
        /// Odošle na server požiadavku o vytvorenie jazyka podľa parametra languageName.
        /// </summary>
        /// <param name="languageName">Názov nového jazyka</param>
        public async Task<bool> AddLanguage(string languageName)
        {
            var language = new Language() {Name = languageName};
            var response = await _http.PostAsJsonAsync($"{_configuration["ApiServerUrl"]}api/Languages", language);

            return response.IsSuccessStatusCode;
        }
    }
}