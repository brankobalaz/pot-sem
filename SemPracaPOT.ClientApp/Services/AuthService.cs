﻿using System.Net.Http.Headers;
using System.Net.Http.Json;
using Blazored.SessionStorage;
using SemPracaPOT.Shared.Models.User;

namespace SemPracaPOT.ClientApp.Services
{
    /// <summary>
    /// Služba na autentifikáciu používateľa.
    /// </summary>
    public class AuthService
    {
        private const string LoginResponseStorageName = "loginResponse";

        private readonly ISyncSessionStorageService _storage;
        private readonly HttpClient _http;
        private readonly IConfiguration _configuration;
        private bool _isLoggedIn;

        public bool IsLoggedIn
        {
            get => _isLoggedIn;
            set
            {
                if (value != _isLoggedIn)
                {
                    _isLoggedIn = value;
                    NotifyStateChanged();
                }
            }
        }

        public LoginResponseDto LoginResponse { get; set; }
        private void NotifyStateChanged() => OnChange?.Invoke();

        /// <summary>
        /// Udalosť je vyvolaná zmenou stavu prihlásenia používateľa.
        /// </summary>
        // Boli použité poznatky z:
        // https://stackoverflow.com/questions/59629257/change-visibility-of-nav-item-in-blazor-menu
        public event Action OnChange;

        /// <summary>
        /// Vráti inštanciu AuthService
        /// </summary>
        public AuthService(ISyncSessionStorageService storage, IConfiguration configuration, HttpClient httpClient)
        {
            _storage = storage;
            _http = httpClient;
            _configuration = configuration;
        }

        /// <summary>
        /// Inicializuje stav prihlásenia používateľa.
        ///
        ///
        /// Skontroluje session storage voči existencií autentifikačného tokenu po poslednom prihlásení.
        /// </summary>
        public void Initialize()
        {
            var lastLoginResponse = _storage.GetItem<LoginResponseDto>(LoginResponseStorageName);

            if (lastLoginResponse is {Token: { }})
            {
                RegisterAuthToken(lastLoginResponse.Token);
                LoginResponse = lastLoginResponse;
                IsLoggedIn = true;
            }
        }

        /// <summary>
        /// Zapíše autentifikačný token do session storage.
        /// </summary>
        private void RegisterAuthToken(string token)
        {
            _http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }

        /// <summary>
        /// Odošle na server požiadavku o schválenie prihlásenia.
        ///
        /// V prípade úspechu je vrátený autentifikačný token, ktorý sa zapíše do session storage.
        /// </summary>
        public async Task<bool> LogIn(string username, string password)
        {
            var loginDto = new LoginDto() {Username = username, Password = password};
            using var responseDto =
                await _http.PostAsJsonAsync($"{_configuration["ApiServerUrl"]}api/Auth/login", loginDto);

            if (responseDto.IsSuccessStatusCode)
            {
                var loginResponse = await responseDto.Content.ReadFromJsonAsync<LoginResponseDto>();

                if (loginResponse is {Token: { }})
                {
                    RegisterAuthToken(loginResponse.Token);
                    _storage.SetItem(LoginResponseStorageName, loginResponse);

                    LoginResponse = loginResponse;
                    IsLoggedIn = true;
                    return true;
                }
            }

            IsLoggedIn = false;
            return false;
        }

        /// <summary>
        /// Odhlási používateľa.
        ///
        /// Odstráni záznam o prihlásení zo session storage.
        /// </summary>
        public void LogOut()
        {
            _storage.RemoveItem(LoginResponseStorageName);
            _http.DefaultRequestHeaders.Authorization = null;
            LoginResponse = null;
            IsLoggedIn = false;
        }

        /// <summary>
        /// Odošle na server požiadavku o registráciu nového používateľa.
        /// </summary>
        public async Task<bool> Register(RegisterDto registerDto)
        {
            var response =
                await _http.PostAsJsonAsync($"{_configuration["ApiServerUrl"]}api/Auth/register", registerDto);

            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Overí, či súčasne prihlásený používateľ disponuje danou rolou.
        /// </summary>
        public bool HasRole(string roleName)
        {
            return LoginResponse != null && LoginResponse.Roles.Contains(roleName);
        }
    }
}