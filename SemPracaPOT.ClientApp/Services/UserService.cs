﻿using System.Net.Http.Json;
using SemPracaPOT.Shared.Models.User;

namespace SemPracaPOT.ClientApp.Services
{
    /// <summary>
    /// Služba na správu používateľov.
    /// </summary>
    public class UserService
    {
        private readonly HttpClient _http;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// vráti inštanciu UserService
        /// </summary>
        public UserService(HttpClient http, IConfiguration configuration)
        {
            _http = http;
            _configuration = configuration;
        }

        /// <summary>
        /// Vráti zoznam všetkých používateľov v databáze servera.
        /// </summary>
        public async Task<List<UserRole>> GetAllUsers()
        {
            var response = await _http.GetAsync($"{_configuration["ApiServerUrl"]}api/User");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadFromJsonAsync<List<UserRole>>() ?? new();
            }
            else
            {
                return new();
            }
        }

        /// <summary>
        /// Pridá/Odoberie používateľovi rolu zadanú parametrom.
        /// </summary>
        /// <param name="username">Používateľské meno</param>
        /// <param name="role">Názov role</param>
        /// <param name="hasRole">Má mať používateľ rolu?</param>
        /// <returns></returns>
        public async Task<bool> SetRole(string username, string role, bool hasRole)
        {
            var setRoleDto = new SetRoleDto() {Username = username, RoleName = role, HasRole = hasRole};

            var response =
                await _http.PostAsJsonAsync($"{_configuration["ApiServerUrl"]}api/User/setRole", setRoleDto);

            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Odošle na server požiadavku o odstránenie používateľa.
        /// </summary>
        /// <param name="username">Používateľské meno</param>
        public async Task<bool> DeleteUser(string username)
        {
            var response =
                await _http.DeleteAsync($"{_configuration["ApiServerUrl"]}api/User/{username}");

            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Vráti zo servera informácie o používateľovi identifikovanom pomovou používateľského mena.
        /// </summary>
        /// <param name="username">Používateľské meno</param>
        public async Task<EditUserDto> GetUser(string username)
        {
            var response = await _http.GetAsync($"{_configuration["ApiServerUrl"]}api/User/{username}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadFromJsonAsync<EditUserDto>();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Odošle na server požiadavku o úpravu používateľa podľa parametrov v newUser.
        /// </summary>
        /// <param name="username">Používateľské meno</param>
        /// <param name="newUser">Nové parametre používateľa</param>
        public async Task<bool> UpdateUser(string username, EditUserDto newUser)
        {
            var response =
                await _http.PutAsJsonAsync($"{_configuration["ApiServerUrl"]}api/User/{username}", newUser);
            return response.IsSuccessStatusCode;
        }
    }
}