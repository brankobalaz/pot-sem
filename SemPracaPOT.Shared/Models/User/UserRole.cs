﻿namespace SemPracaPOT.Shared.Models.User
{
    public record UserRole
    {
        /// <summary>
        /// Záznam modeluje dáta jedného používateľa.
        ///
        /// Obsahuje zoznam rolí, ktoré má používateľ pridelené.
        /// </summary>
        public List<string> Roles { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
}