﻿using System.ComponentModel.DataAnnotations;

namespace SemPracaPOT.Shared.Models.User
{
    /// <summary>
    /// Záznam modeluje dáta potrebné pre prihlásenie používateľa.
    /// </summary>
    // Boli použité poznatky z:
    // https://www.c-sharpcorner.com/article/authentication-and-authorization-in-asp-net-core-web-api-with-json-web-tokens/
    public record LoginDto
    {
        [Required(ErrorMessage = "User Name is required")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
    }
}
