﻿namespace SemPracaPOT.Shared.Models.User
{
    /// <summary>
    /// Záznam modeluje dáta odpovede servera na prihlásenie používateľa.
    /// </summary>
    public record LoginResponseDto
    {
        public string Id { get; set; }
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
        public string Username { get; set; }
        public ICollection<string> Roles { get; set; }
    }
}
