﻿namespace SemPracaPOT.Shared.Models.User
{
    public record SetRoleDto
    {
        /// <summary>
        /// Záznam modeluje dáta potrebné pre úpravu role používateľa.
        /// </summary>
        public string Username { get; set; }
        public string RoleName { get; set; }
        public bool HasRole { get; set; }
    }
}