﻿using System.ComponentModel.DataAnnotations;

namespace SemPracaPOT.Shared.Models.User
{
    /// <summary>
    /// Záznam modeluje dáta potrebné pre registráciu používateľa.
    /// </summary>
    // Boli použité poznatky z:
    // https://www.c-sharpcorner.com/article/authentication-and-authorization-in-asp-net-core-web-api-with-json-web-tokens/
    public record RegisterDto
    {
        [Required(ErrorMessage = "User Name is required")]
        public string Username { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        [StringLength(20, ErrorMessage = "Must be between 8 and 20 characters", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
    }
}