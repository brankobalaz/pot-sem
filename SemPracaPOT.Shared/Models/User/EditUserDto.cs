﻿using System.ComponentModel.DataAnnotations;

namespace SemPracaPOT.Shared.Models.User
{
    /// <summary>
    /// Záznam modeluje dáta pre editáciu používateľa.
    /// </summary>
    public record EditUserDto
    {
        public string Username { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Password { get; set; }
    }
}