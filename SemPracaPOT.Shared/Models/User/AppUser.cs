﻿using Microsoft.AspNetCore.Identity;

namespace SemPracaPOT.Shared.Models.User
{
    /// <summary>
    /// Používateľ používaný v aplikácií.
    ///
    /// Slúži ako predpríprava pre rozširovanie aplikácie.s
    /// </summary>
    public class AppUser : IdentityUser
    {
    }
}