﻿namespace SemPracaPOT.Shared.Models
{
    /// <summary>
    /// Záznam modeluje odpoveď servera (najčastejiše chybovú odpoveď).
    /// </summary>
    public record ResponseDto
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}