﻿namespace SemPracaPOT.Shared.Models.Dictionary
{
    /// <summary>
    /// Záznam modeluje dáta prekladu.
    /// </summary>
    public record TranslationDataDto
    {
        public Word Word { get; set; }
        public int WordId { get; set; }
        public int LanguageIdTo { get; set; }
        public List<Translation> Translations { get; set; }
    }
}
