﻿namespace SemPracaPOT.Shared.Models.Dictionary
{
    /// <summary>
    /// Záznam modeluje krížovkársku definíciu slova.
    /// </summary>
    public record CrosswordDefinition : DatabaseObject
    {
        public int Id { get; set; }
        public string Definition { get; set; }
        public int WordId { get; set; }
        public Word Word { get; set; }
    }
}
