﻿namespace SemPracaPOT.Shared.Models.Dictionary
{
    /// <summary>
    /// Záznam modeluje vťah prekladu medzi dvomi slovami.
    /// </summary>
    public record Translation : DatabaseObject
    {
        public int WordId { get; set; }
        public Word Word { get; set; }
        public int TranslationWordId { get; set; }
        public Word TranslationWord { get; set; }
    }
}
