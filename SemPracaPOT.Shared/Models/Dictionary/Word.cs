﻿namespace SemPracaPOT.Shared.Models.Dictionary
{
    /// <summary>
    /// Záznam modeluje slovo v databáze.
    /// </summary>
    // Boli použité poznatky z: 
    // https://stackoverflow.com/questions/12237617/entityframework-same-table-many-to-many-relationship
    public record Word : DatabaseObject
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public int LanguageId { get; set; }
        public Language Language { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Translation> Translations { get; set; }
        public virtual ICollection<Translation> TranslationsOf { get; set; }
        public virtual ICollection<CrosswordDefinition> CrosswordDefinitions { get; set; }
    }
}