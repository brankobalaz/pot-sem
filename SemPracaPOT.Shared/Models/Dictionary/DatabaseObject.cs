﻿using SemPracaPOT.Shared.Models.User;

namespace SemPracaPOT.Shared.Models.Dictionary
{
    /// <summary>
    /// Záznam pokýva informácie o čase vytvorenia a vlastníctve záznamu.
    ///
    /// Predok všetkých záznamov v databáze.
    /// </summary>
    public record DatabaseObject
    {
        public DateTime CreatedAt { get; set; }
        public string AppUserId { get; set; }
        public AppUser AppUser { get; set; }
    }
}