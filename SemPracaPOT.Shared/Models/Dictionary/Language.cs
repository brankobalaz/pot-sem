﻿namespace SemPracaPOT.Shared.Models.Dictionary
{
    /// <summary>
    /// Záznam modeluje databázový objekt jayzka.
    /// </summary>
    public record Language : DatabaseObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}